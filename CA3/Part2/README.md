# CA 3 - Practice Class:
# Part 2

This part of the Class Assignment is to use Vagrant setup to execute a spring boot tutorial, with the Gradle Basic 
Version.

# Vagrant

Vagrant helps to define the configuration of a VM( Virtual Machine) using a file configuration (usually called vagrant 
file), for example, using Virtual Box (the VM installed in the previous class).
One of the advantages points of Vagrant is, if we want to share the VM, just needed to shate the configuration file.

Let's start to set up the vagrant,so please try to follow the tutorial.

# Install and "Play" with Vagrant

## Step 1

Go to https://www.vagrantup.com/downloads.html, download the version for your OS(Operating System) and install it.

After that, open terminal, and type the following command:

~~~
vagrant -v

OR

vagrant --version
~~~

This will allow you the check your version of vagrant.

## Step 2

Create a folder, with:

~~~
mkdir vagrant-project-1
~~~

And go to that new folder:

~~~
cd vagrant-project-1
~~~

## Step 3

Let's create a Vagrant configuration file. Type the following command that will allow you to setup a vagrant project 
based on ubuntu 16.04 box:

~~~
vagrant init envimation/ubuntu-xenial
~~~

To "acelerate" the process type instead:

~~~
vagrant box add envimation/ubuntu-xenial
~~~

## Step 4

To start the VM:

~~~
vagrant up
~~~

## Step 5

To start a ssh version:

~~~
vagrant ssh
~~~

## Step 6

Let's initialize the configuration file, the VagrantFile:

~~~
vagrant init envimation/ubuntu-xenial
~~~

An ubuntu-xenial is the name of the Vagrant box, with a preconfiguration. The VagrantFile is composed with all the necessary
information about the VM configuration.
Most of the configurations starts with #, because they are commented, and all of this file can be changed.

## Step 7

To download the vagrant box, calls the hypervisor to create the VM, installs all the hypervicor tools, runs provision and
setups all ssh keys (to do the login), type:

~~~
vagrant up
~~~

## Step 8

If you want to shutdown the VM, press:

~~~
vagrant halt
~~~

## Step 9

If we want to change the vagrant file, we need to write the following command:

~~~
nano vagrantfile
~~~

and uncomment some of the lines ou add command as ypu need and want. 

## Step 10

To install Apache, please, uncomment the following line, at the vagrant file:

~~~
cnfig.vm-provision
~~~

And change the port actual port:

~~~
host:8080
~~~

to

~~~
host:8010
~~~

## Step 11

Uncomment the following lines:

~~~
private_network

AND

config.vm.synced_folder

~~~

and change the ip, from:

~~~
ip:192.168.33.10
~~~

to

~~~
ip:192.168.56.5
~~~

and 

~~~
../data
~~~

to

~~~
/vagrant_data
~~~

And change:

~~~
./html
~~~

to 

~~~
/var/www/html
~~~

At the end, press:

~~~
Crtl + X
~~~

to save and leave the file.

## Step 12

At the end of this steps, write:

~~~
vagrant reload --provision
~~~

To reload all the changes and remove the box from the machine, with:

~~~
vagrant box remove envimation/ubuntu-xenial
~~~

## Step 13

Clone the following repository:

~~~
git clone https://Jo_Silva@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git
~~~

to the /CA3/Part2.

## Step 14

Open the vagrantfile in IDE and add this changes:

~~~
rm -rf devops-21-22-lmn-1211770
git clone https://Jo_Silva@bitbucket.org/jo_silva/devops-21-22-lmn-1211770.git
cd devops-21-22-lmn-1211770/CA2/Part2/react-and-spring-data-rest-basic
chmod u+x gradlew
./gradlew clean build
~~~

and also change line 12 from:

~~~
sudo apt-get install openjdk-8-jdk-headless -y
~~~

to

~~~
sudo apt-get install openjdk-11-jdk-headless -y
~~~

## Step 15

This Vagrant file, from vagrant-multi-spring-tut-demo, will allow to create two VM:
 - A web (to run tomcat and spring boot basic application);
 - A db (to run the H2 database server).

## Step 16

Send it to the repository with:

* git status (to see the files that were changed);
* git add . (to add the files that want to send to bitbucket);
* git commit -m "MESSAGE" (don't forget the associated issue);
* git push.

## Step 17

The application needs to undergo some changes and to fast the process copy the /src folder and replace the actual /src
folder. (I didn´t delete the first /src folder so i called "old_src"). To do that i clone the Professor Alexandre Bragança 
project, with the:

~~~
git clone https://Jo_Silva@bitbucket.org/atb/tut-basic-gradle.git
~~~

## Step 18

Start the VM with:

~~~
vagrant up
~~~

## Step 19

To reload the new changes, type:

~~~
vagrant reload --provision
~~~

## Step 20

To test the VM (web ) go to:

* http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/

![](Image/img_0.png)

To test the VM (db) go to:

* http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/h2-console

![](Image/img_1.png)

## Step 21

Try to do a simple query in the H2-console:

~~~
select * from employee
~~~

![](Image/img_2.png)

## Step 22

Try to add two rows at the H2-console:

~~~
insert into employee (id, description, first_name, last_name) values (2, 'DevOps Student', 'Joana', 'Silva')
~~~

![](Image/img_3.png)

and

~~~
insert into employee (id, description, first_name, last_name) values (3, 'DevOps Student', 'Isabel', 'Miguel')
~~~

![](Image/img_4.png)

## Step 23

Check all the changes at the:

* http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/

![](Image/img_5.png)

## Step 24

Send all the work done to the repository with:

* git status (to see the files that were changed);
* git add . (to add the files that want to send to bitbucket);
* git commit -m "MESSAGE" (don't forget the associated issue);
* git push.


---

# CA 3 - Practice Class:
# Alternative

At this point, i will show the best 5 alternatives to VirtualBox that i found and a short conclusion.

### VMware Workstation Player
 - It can be used to create and run virtual machines without changing the operating systems;
 - Cannot run 3 virtual machines at the same time; 
 - It isn't "Mac friendly" because is only available for Windows and Linux.

### VMware Fusion/Pro
 - Fusion Pro is OS only, so you can run Windows and Linux in a virtual machine on your Mac.

### Parallels Desktop
- It offers two versions for individuals and businesses;
- Parallels Desktop does everything VMware Fusion does, but with a few nuances;
- It also allows simultaneous execution of Mac and Windows;
- Parallels is not limited to Windows and can run ChromeOS, Linux, and Mac;
- It also supports Windows 10, supports Cortana integration;
- Is easy to use, fast in speed, and powerful. 
- Can run thousands of Windows apps like Microsoft Access, Microsoft Office, Quicken, Internet Explorer, QuickBooks and others.

### Portable VirtualBox
- Is a free, open-source software tool that allows you to run all operating systems on a USB stick without a separate installation;
- With Portable VirtualBox, unzip the VirtualBox, adjust the path and configuration, and run it.

### QEMU
- Quick Emulator (QEMU);
- Is a powerful free and open-source virtualization platform;
- It works with Mac OS X, Windows, and Linux as a server or client and is quite stable;
- It is open-source;
- It is very easy to configure and install.


### Conclusion
The 5 alternatives for VirtualBox work quite well and had lots of advantages. All of them has strengths and weaknesses, 
but is Parallels Desktop and VMWare that are the most recommend, with easy use and intuitive features.