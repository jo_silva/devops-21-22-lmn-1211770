# CA 3 - Practice Class:
# Part 1

The main objective in this class is to learn how to create a VM (Virtual Machine) and try to execute the spring boot 
tutorial basic and the gradle_basic_demo project.


## What is a Virtual Machine?

A Virtual Machine, as the name says, is a technique of virtualization (or emulation) of computer system, with several 
hardware devices, using a hypervisor. 
A hypervisor may have many virtual hardware and operating system, and both combined make a virtual machine (VM).


## What types of Virtualizations are there?

There are two types of virtualizations, a hardware-level virtualization (the hypervisor executes directly on the 
hardware) and the host virtualization (the hypervisor needs to execute on an operating system).


## Which Virtualization Software we are going to use in this tutorial?

Exist many different hypervisors but, in this tutorial, we are going to use the VirtualBox.


## Some Advantages of Virtualization:

* Execute the same VM, with different operating systems;
* Supports legacy operating systems;
* Management of several projects in a restricted environment;
* Less physical hardware;
* Eco-friendly.


## Some Disadvantages of Virtualization:

* May be complex at some times;
* The costs may be higher;
* May be less efficient;
* Must use multiple links, un a chain, that must work together cohesively;
* When a VM has several hosts, the performance maybe weaker. 


## Quick tutorial to install the VM (try to follow all the steps):

1. Download VirtualBox from the following link: https://www.virtualbox.org/
2. If you open the file, that is now at your computer, you will see the specification of the VM;
3. Press "new" button;
4. Will open a text box to fill with the name of the VM, the folder where it will be holded, the type of the operating
system (choose "Linux") and the version of the operating system (choose "Ubuntu (64-bit)");
5. Press "continue";
6. Then will appear a box with the hardware information (memory size), de fault value is 1024 MB but, please, switch to 
2048 MB of RAM;
7. A box with the information of hard disk will be showed, and it will be suggested a size of 10GB for the disk. Remember
that this is virtual, so this 10GB will not affected your machine. Try to press "create a virtual hard disk now";
8. Press "Creat";
9. Then a technical operation will appear (to choose a type of hard disk file), please choose VDI (VirtualBox Disk
Image);
10. For the storage on physical hard disk choose "Dynamically allocated" (it means that the disk will be used as its 
needed).
11. The file location will appear, accept the path ou change it;
12. Now the machine is created! Bit it's needed to add more features, so press "Start";
13. A message will appear, with a fatal error, so ir is needed to connect a image (ISO) with the Ubuntu 18.04;
14. Try to follow this link: https: //help.ubuntu.com/community/ Installation/MinimalCD ;
15. Download the Ubuntu image with the 64 bits;
16. Now press "Settings" and go to "Storage". You will see that the "Controller:IDE" is empty. So press the CD button,
with the right button from your mouse, and the file "mini.iso" (that you downloaded in the previous step) will appear;
17. Choose the "mini.iso" file and click in the "Live CD/DVD" (this will simulate the step of insert the CD in the 
computer) and press "ok";
18. Start the VM again;
19. The following step is to install ubuntu, so please press "Install";
20. Choose the main language of the VM (portuguese from Portugal);
21. To configure the keyboard, press "Yes";
22. And, at this point, press one of the following letters that will appear or respond with yes/no to the questions. 
When the configuration will end, the VM will suggest that the Keyboard is a portuguese keyboard, press "Configure";
23. To configure the network, will suggest the "name of the computer" with the ubuntu name, press "yes";
24. Choose "Portugal" in the next step;
25. Then will appear to configure the proxy, just press "enter", do not write anything, please;
26. A download bar will appear, please wait;
27. Then it will be needed to write the name of the host os the VM, write your first name and last name;
28. Then write the name of the username;
29. Choose a password and memorize it (it will be needed);
30. Then you will need to configure the time zone (choose Lisbon time zone);
31. Then will appear the partition of the disk, so please choose "Guided - Use all disk" and then confirm it;
32. Press "yes" to install the base system (a download bar will appear, please wait);
33. To configure PAM, choose "Without configurations";
34. To select software, please, don't select anything;
35. A message will appear for the GRUB hard disk (software that will allow to manage several operating systems) press 
"yes";
36. The last message is about the o'clock, that is defined has UTC, so press "yes";
37. Press "Continuar" and the basic installation is completed, but we need to "take out" the CD (the mini.iso file). So
go to the "Devices" button and press "mini.iso" (the symbol will disappear);
38. Start the VM again, write your username and your password;
39. Now we are going to configure the network, go to the "Settings", then to "Network" and on the "attached to:" choose
NAT and press "ok";
40. On the VM settings choose "File" and "Host Network Manager";
41. A list will appear, so press the first one. If the list is empty, press "create" and choose the network that you 
created;
42. Then choose, on that network, de DHCP "Enable" and press "close";
43. Shut down the VM (with the power off button);
44. Go to the settings and then to "Network", and press "Adapter 2" and on the "attached to:" choose "Host-only Adapter" 
and press "ok";
45. Now the VM is connected to the network;
46. Start the VM again and write your credentials;
47. Now we are going to continue the setup. please write the following commands:
48. Write "sudo apt update". Is a command to install new software (sudo means "super user do");
49. It will need to write your credentials;
50. Write "sudo apt install net-tolls". Command to install the package net-tools, that are tools to manage the network;
51. Write "sudo nano /etc/netplan/01-netcfg.yaml", with this command we can edit the network configuration file to 
set up the ip. Nano is a text editor. The path "etc/netplan", is a file to configure the network. This file will be 
changed
52. "enp0s3" is the first interface that allows access to the internet (which was as nat, in the VM settings);
53. Try to copy and past this:
~~~
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
     dhcp4: yes
    enp0s8:
     addresses:
       - 192.168.56.5/24
~~~
Now we have the address of the second network card.

54. Press "Control" + "O" to save and press "Control" + "X" to leave the text editor;
55. Write "sudo netplan apply", to apply the new changes;
56. Write "sudo apt install openssh-server", to remotely enter the VM;
57. Write "ssh server", access remotely the VM;
58. Write "sudo nano/etc/ssh/sshd_config", another configuration so that we can access remotely and enter with the 
authentication;
59. In this text editor we need to uncomment "PasswordAuthentication yes" (initially, the color of the line appears 
60. blue (because it is commented) but later it will turn white because it is uncommented);
61. Press "Control" + "0" to save and press "Control" + "X" to leave the text editor;
62. Write "sudo service ssh restart", to restart ssh, and so we can access the machine remotely;
63. Write "sudo apt install vsftpd", to install an ftp server so that we can use the FTP protocol to transfers files;
64. In this text editor we need to uncomment "write_enable=YES" (initially, the color of the line appears
blue (because it is commented) but later it will turn white because it is uncommented);
65. Write "sudo service vsftpd restart", to check the new configurations;
66. Now wee will pass to the local terminal (terminal from our physical machine);
67. Write" ssh username@192.168.56.5", and substitutes "username" from your username;
68. Write the password to access the VM;
69. Now we need to download thw "FileZilla", with this link: https://filezilla-project.org/ ;
70. When downloaded, open the FileZilla and write your ip host (192.168.56.5), the username and the password to access 
the VM;
71. Write "sudo apt install git", to install the Git at the VM;
72. Write "sudo apt install openjdk-11-jdk-headless", to install Java, version 11;
73. Now we can write "git -version" to chechk the version that was installed;
74. Now we can clone our project from bitbucket:
~~~
git clone https://Jo_Silva@bitbucket.org/jo_silva/devops-21-22-lmn-1211770.git
~~~
75. All the configurations are now installed, so we can pass to the exercises.


## Build and execute spring-boot tutorial basic project:

* It is needed to go to the tut-react-and-spring-data-rest, so:
~~~
cd devops-21-22-lmn-1211770 

cd CA1

cd tut-react-and-spring-data-rest

cd basic 
~~~ 

* Now we are ready to run the spring boot:

~~~
./mvnw spring-boot:run
~~~

* Let the application run, then go to the browser and try to see if the information changed in CA1 will appear. To see 
that write:

~~~
http://192.168.56.5:8080/
~~~

* We can't write "http://localhost:8080/" because we are executing spring boot from the VM so we need to write, instead
of localhost, the IP from our VM to see the changes.


## Build and execute gradle_basic_demo project:

At first, we can run the gradle with:

~~~
./gradlew build
~~~

After that option, we can use :

~~~
./gradlew runServer
~~~

The VM will run the server and our machine (our PC) is going to run the client with the comand:

~~~
./gradlew runClient
~~~

For that it was needed to change the runClient tasks:

~~~
task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on localhost:59001 "
  
    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
}
~~~

And now the tasks was able to run.
