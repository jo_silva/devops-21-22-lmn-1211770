# CA 5 - Practice Class:
# Part 2

The main objective in this class is to create a pipeline in Jenkins to build the tutorial spring boot 
application, gradle "basic" version.


# Try to follow the steps to achieve the goal:

1. We are going to use our Jenkins, but to accomplish this task we need to install this plugins:

- HTML Publisher (because we need to publish the javadoc);
- Docker Pipeline (because we are using a dockerfile to compile).

2. To do this, go to:

- Dashboard;
- Manage Jenkings;
- Manage Plugins;
- Available;
- In the "magnifying glass" symbol write the plugins and install them.

3. At the end of the plugins are installed, restart the Jenkins to refresh the plugins.

4. To give permissions to Jenkins to access Dockerhub, we need to create the credentials in Jenkins. So, to do this, go to:

- Dashboard;
- Manage Jenkins;
- Manage Credentials;
- Must be "Global";
- Add Credentials;
- Username (give the username of DockerHub);
- Password (give the password of DockerHub);
- Define the name you wan to give to this credentials, in my case i called it "dockerhub_credentials";
- Save.

5. Create a "new item", with this information:

- Add the name "ca5-part2";
- Select "pipeline";
- Select "Pipeline script from SCM", and choose the repository that we want to use;
- SCM: Git, (Source Control Management), what is used to clone our project;
- Repository URL: https://Jo_Silva@bitbucket.org/jo_silva/devops-21-22-lmn-1211770;
- Credentials: the repository is public;
- Branches to build: master;
- Script Path CA5/Part2/Jenkinsfile, path to check the Jenkisfile.


6. Create a txt file for Jenkinsfile, with this information:

~~~
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://Jo_Silva@bitbucket.org/jo_silva/devops-21-22-lmn-1211770'
            }
        }
        stage('Build') {
            steps {
                echo 'Assembling...'
                dir('CA5/Part2/gradle_basic_demo') {
                sh './gradlew clean assemble'
                }
            }
        }
        stage('Test') {
                    steps {
                        echo 'Testing...'
                        dir('CA5/Part2/gradle_basic_demo') {
                        sh './gradlew test'
                        }
                    }
                }
        stage('Generate Jar') {
                   steps {
                        echo 'Generating jar...'
                        dir('CA5/Part2/gradle_basic_demo') {
                        sh './gradlew jar'
                        }
                   }
                }
        stage('Javadoc') {
                  steps {
                        echo 'Creating Javadoc...'
                        dir('CA5/Part2/gradle_basic_demo') {
                        sh './gradlew javadoc'
                        }
                   }
                }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                        dir('CA5/Part2/gradle_basic_demo') {
                archiveArtifacts 'build/distributions/*'
                }
            }
        }
        stage('Docker Image') {
                    steps {
                        echo 'creating and publish...'
                        dir('CA5/Part2/gradle_basic_demo') {
                        script {
                        docker.withRegistry('https://registry.hub.docker.com', 'dockerhub_credentials') {
                        def image = docker.build("1211770/ca5part2:${env.BUILD_ID}")
                        image.push()
                            }
                        }
                        }
                    }
                }
    }
    post {
            always {
                dir('CA5/Part2/gradle_basic_demo') {
                junit 'build/test-results/test/*.xml'
                }
                publishHTML([allowMissing: false,
                            alwaysLinkToLastBuild: false,
                            keepAll: false, reportDir: 'CA5/Part2/gradle_basic_demo/build/docs/javadoc',
                            reportFiles: 'index.html',
                            reportName: 'HTML Report',
                            reportTitles: 'The Report'])
                            }
                }
}
~~~

7. Create a txt file for Dockerfile, with this information:

~~~
FROM ubuntu:18.04

RUN apt-get update -y
RUN apt-get install openjdk-11-jdk-headless -y

COPY build/libs/basic_demo-0.1.0.jar .

EXPOSE 59001

CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
~~~

8. After that, don't forget to do:

~~~
git add .
~~~

AND

~~~
git commit -m"message"
~~~

AND

~~~
git push
~~~

9. Then go to Jenkins, to the "ca5-part2" item, press "build now" and analyse the results:

![](Images/build_success.png)

10. Don't forget to tag you last commit, like this:


~~~
git tag ca5-part2
~~~

And, at last:

~~~
git push origin ca5-part2
~~~
