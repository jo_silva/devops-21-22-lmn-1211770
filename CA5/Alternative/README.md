# CA 5 - Practice Class:
# Alternative


Jenkins vs Ansible


## Ansible


Ansible is an open-source tool for service employment. It is a solution for software provisioning, application deployment, 
and configuration management. There are no requirements for setting up separate management structures. 


### Advantages of Ansible


- ADVANTAGES:

* Cloud Oriented;
* Flexible;
* Easy to learn;
* Super efficient and flexible;
* Vagrant provisioner.


## Jenkins

Jenkins is an open-source CI server built with Java for automation and Continuous Integration tasks. Users can create 
and test projects while integrating changes quickly. Has a large number of plugins which work well with CI/CD tools. 
It can handle creation, testing, packaging, deployment, analysis, and other operations. It is a preferred choice of many 
DevOps teams. 


### Advantages of Jenkins


 - ADVANTAGES:

 * Open Source and Free;
 * Plug-ins and Integration;
 * Hosting Option;
 * Community Support;
 * Easy to debug;
 * Less time to deliver the project.


### Ansible and Jenkins


While both Ansible and Jenkins are useful for supporting teams that want to follow DevOps principles, the two applications 
are built for significantly different use cases.
Ansible will likely be a good fit for teams that need IT automation software capable of handling cloud environments.
Jenkins is an excellent CI/CD tool that can be extended significantly with the right plugins. Teams that are aiming for 
CI/CD or that have complex tech stacks they need to consider will likely benefit more from Jenkins.
It is also possible to use both Jenkins and Ansible together. Setting up and configuring these tools may be a challenge, 
though, compared to other IT automation tool combinations.


### Differences


![](Images/vs.png)

