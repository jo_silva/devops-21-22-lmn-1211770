# CA 5 - Practice Class:
# Part 1

The main objective in this class is to practice with Jenkins using the "gradle basic demo" project that should 
already be present in the individual repository.

# Try to follow the steps to achieve the goal:

1. Download the jenkins package through https://jenkins.io/download/ and choose the generic package

![](Images/Generic Package.png)

2. Place it into a folder for your preferences (e.g. the applications folder);
3. Open terminal and go to the source of that folder;
4. Then write the following command:

~~~
java -jar jenkins.war
~~~
to execute directly the war file

![](Images/java_jenkins.png)

5. Then go to the http://localhost:8080 and follow the instructions bellow:
 - In the terminal you will find a password

![](Images/password.png)
 
 - Install all the suggested plugins (choose the default package)

![](Images/Installing Plugins.png)

 - Then the installation process is going to start

![](Images/default plugins.png)

 - Create and user account

![](Images/user.png)

 - Instance configuration (select the localhost)

![](Images/localhost.png)

 - then the proceed ended with this message

![](Images/created.png)

6. Now let's check some features:

- go to the "manage jenkins" and press "manage plugins" 

![](Images/credentials.png)

If there is needed some plugin to this work try to download and install it;

- In the "global tool configuration", you can install git, maven, gradle ... you can add it to the projects;

![](Images/global configuration.png)
![](Images/add java.png)

- In the "manage credentials", you can to access some repositories (public or private)

7. Is going to exist a Jenkins file ( similar to vagrant file or docker file, that uses groovy and is to store the pipeline.
Usually, is allocated in the root folder);

8. Go this link https://gist.github.com/atb/bb88c4231f095a8bd8566b5dab415850

9. Copy the "gistfile1.txt"

10. Then go to Jenkins Dashboard

11. Press "new item" 

12. Write the name of the job (e.g. devops-21-22-LMN-1211770), press "pipeline" and "ok"(imagem);

![](Images/job devops.png)

13. Is going to open a configuration dashboard, for the configuration of the job, and scroll down until you find the 
"pipeline" step and paste the txt copied in the 9 step, and press "save";

![](Images/job configuration.png)

14. Is going to be presented to you a page with the pipeline information and, because it hasn´t have a build process, to
test it, press "build  now";

15. The pipeline is going to build and appears a stage view, like this:

![](Images/inside build.png)

16. Then you can see the console output to check the information:

![](Images/console output.png)

This was an simple example, like a "hello world!" for the pipeline creation;

17.Now we need to "step up" a little!
Start to create a job (i named it "devops-21-22-joana") (imagem), choose "pipeline" and press "ok";

![](Images/new job.png)

18. Now it will appear a new configuration form, go to the "pipeline" and paste the following script:

~~~
pipeline { 
    agent any
    stages { 
        stage(’Checkout’) {
            steps {
                echo ’Checking out...’
                git ’https://bitbucket.org/luisnogueira/gradle_basic_demo’
            }
        }
        stage(’Build’) {
            steps {
                echo ’Building...’
                sh ’./gradlew clean build’
            }
        }
        stage(’Archiving’) { 
            steps {
                echo ’Archiving...’
                archiveArtifacts ’build/distributions/*’ 
            }
        }
    }
}
~~~

(you will find this information on the pipeline at https://gist.github.com/atb/d23ce58350c4df272473c7fd7a96838f)

This is a different script that is going to pull the project from Prof Luis Nogueira, then is going to build it (invoking
gradle) and at last, is going to archive the files in the "archiveArtifacts";

![](Images/new script.png)

19. Then press "build now" to execute the job;

20. The build was a success;

![](Images/build success.png)

21. Now in a different way, we are going to the Prof Luis Nogueira project ( gradle-build-demo) and we are going to make
a copy from this and add a jenkins file to the configuration

22. In the repository ( https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/), press "fork this repository"

![](Images/fork.png)

23. then write your workspace, the project, the name (i named it "gradle-basic-demo-jenkins) and choose the private repository;

24. the fork is running;

![](Images/running.png)

25. Now i have, in my repository, a copy of the project;

![](Images/gradle jenkings.png)

26. Let's clone to our machine the project ( git clone https://Jo_Silva@bitbucket.org/jo_silva/gradle_basic_demo-jenkins.git);

27. Open an editor file txt to build the "Jenkinsfile", and give the file that exact name;

28. Copy this script, from https://gist.github.com/atb/2a5587bd5392dfada81a2f74e811824b:
(this script is different because it has the credentials that must be used, because the repository is private)

~~~
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'atb-bitbucket-credentials', url: 'https://bitbucket.org/atb/gradle_basic_demo'
            }
        }
        stage('Build') {
            steps {
                echo 'Building...'
                sh './gradlew clean build'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }
    }
}
~~~

place this script in the Jenkinsfile that you created in step 29;

29. But, like it was said in the previous step, the repository is private so we need to configure our credentials;

30. Go to the Dashboard, then to "Manage jenkins" and, at last, to "manage credentials";

![](Images/credentials.png)

31. Then press "jenkins", then "Global credentials (unrestricted)" and "add credentials";

![](Images/add credentials.png)

32. And to configure the credentials, please try to follow the advises on the image, and on the private key, press 
"add", and add write your bitbucket password;

![](Images/setup credentials.png)

33. The credentials is now configurated;

![](Images/credentials ok.png)

34. Then go to the jenkingsfile and, on the line number 8, place your id credentials and the place that should be done the
clone of the project (https://bitbucket.org/jo_silva/gradle_basic_demo-jenkins/);

35. Now we are going to try with other script in the pipeline. Create a new job, called "ca5-part1":

~~~
pipeline {
agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git branch : 'master', url: 'https://Jo_Silva@bitbucket.org/jo_silva/devops-21-22-lmn-1211770'
            }
        }
        stage('Build') {
            steps {
                echo 'Assembling...'
                dir('CA5/Part1/gradle_basic_demo') {
                sh './gradlew clean assemble'
                }
            }
        }
        stage('Test') {
                    steps {
                        echo 'Testing...'
                        dir('CA5/Part1/gradle_basic_demo') {
                        sh './gradlew test'
                        }
                    }
                }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                        dir('CA5/Part1/gradle_basic_demo') {
                        archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }
 post {
        always {
           junit 'CA5/part1/gradle_basic_demo/build/test-results/test/*.xml'
        }
      }
}
~~~

36. To configure the script:

- Select "Pipeline script from SCM", and choose the repository that we want to use; 
- SCM: Git, (Source Control Management), what is used to clone our project;
- Repository URL: https://Jo_Silva@bitbucket.org/jo_silva/devops-21-22-lmn-1211770;
- Credentials: the repository is public;
- Branches to build: master; 
- Script Path CA5/Part1/Jenkinsfile, path to check the Jenkisfile;

37. Add in the .gitignore file the line:

~~~
!gradle-wrapper.jar
~~~

Because it is necessary to use the gradlew commands in this file. 

38. Save the changes made, add it to the repository with:

~~~
git add .
~~~
AND
~~~
git commit -m"message"
~~~
AND
~~~
git push
~~~

39. Press "Build Now" and check the results:

![](Images/ok.png)

40. Don't forget to tag your last commit with the "ca5-part1":

~~~
git tag ca5-part1
~~~

And, at last:

~~~
git push origin ca5-part1
~~~


