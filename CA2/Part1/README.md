# CA 2 - Practice Class:
# Part 1

The main objective in this class is to learn about building tools, in this specific case, gradle. 


## What is a Build Tool?

Build tools are programs that automate the creation of executable applications from source code. 
The building concept includes compiling, linking and packaging the code into a usable or executable form.
Nowadays, using an automation tool allows the build process to be more consistent.


## What is a Gradle?

Gradle is free and open source building tool, that uses Groovy. (Groovy is a language with static-typing and static 
compilation capabilities, for the Java platform.)
It is very flexible and extensible because it has plugins for Java, Groovy, Scala and others. 
Has a deep and rich API for managing projects, tasks and dependency artefacts. 


## Gradle Analysis

Gradle is "Task" based (tasks are a sequence of action for the objects). Some tasks are able to have dependencies on
other tasks and may be expected first or last this action. For example, let's see this example:

~~~

task odd {
  group ’devops’
  description ’Example with closures’
}
odd {
  doFirst {
    def isOdd = { int i -> i%2 != 0 }
    println "The number 123 is odd? " + isOdd(123)
} }
odd { doLast {
    println "Bye"
  }
}
~~~

This task is able to check if a number is odd (and print the message). 
If we check the code carefully, we wll see that there one task and two actions. The task has the "odd" name and the actions
are "doFirst" and "doLast". The "doFirst" it means that this tasks needs to be made in the first place and the "doLast"
action needs to be made last.

Another example, is the dependencies between tasks, that it means one task is dependent on another task. Let's see the 
example:

~~~
tas taskY {
    doLast {
       println ’taskY’
    }
}
task taskX {
    dependsOn taskY
    doLast {
       println ’taskX’
    }
}
~~~

This it means that TaskY needs to exist and needs to run before taskX.

## Another Quick Example Build Tool - ANT
One of the first building tools, Apache Ant, is a software tool for automating build processes. It is similar to Make 
(an "old" building tool that remains widely used, especially in Unix operating systems), but implemented using the Java 
language and requires a Java platform. 
It is a free open-source Apache Project and can be downloaded from http://ant.apache.org/


## *Class Exercise*

To accomplish the goal of the class exercise you need to follow this instructions:


### First Step

You need to clone this repository ( https://bitbucket.org/luisnogueira/gradle_basic_demo/ ). To do this, open your 
terminal and go to the folder you want to save the repository. Then do:

* git clone https://Jo_Silva@bitbucket.org/luisnogueira/gradle_basic_demo.git


### Second Step

You need to read, with attention, de README file. 
In the terminal, on the file you have already cloned, the "gradle_basic_demo", do this command to build the java file:

* ./gradlew build

After that, run the server with this command: 

* java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

The 59001 is the port that is assumed, but if you want to use another port (to simulate other IP) use your own IP address
and the IP address from you teammates. 

To run the client, it is needed to open another terminal, in the file root directory, to run the gradle task:

* ./gradlew runClient

After run this task, a chat will open and now to can have a "conversation with yourself".
If you want to chat with your teammates, and after the server has run with their IP's, each one of them will run the 
client task, and then you all can talk.


### Third Step

Open the file "build.gradle" (in this file there are scripts where one can automate the tasks and all the dependencies.
For example, the task to run the server is there), and make a task the run the server that substitutes the command in
the readme file.

First to write the task you need to understand that the task will run the server so, for this, it depends on the 
existence on the java classes. The file is the JavaExec (java execution) type, because all the classes need to run 
correctly.
The classpath is the Java compiler that specifies the location of user-defined classes and packages (and is the same 
classpath from the runClient task). 
The mainClass is 'basic_demo.ChatServerApp' (use both ' in the example) and the args (arguments) is the IP choosen and
used in the second step - 59001.

When you finished the task, it will be with this appearance:

~~~
task runServer(type:JavaExec, dependsOn: classes){

group = "DevOps"

description = "Launches the server"

classpath = sourceSets.main.runtimeClasspath

mainClass = 'basic_demo.ChatServerApp'

args '59001'
}
~~~

After that, go to the terminal and run the task with the following command:

* ./gradlew runServer

The server will run with this command that substitutes the "run server command" in the second step.


### Fourth Step

It is needed to add a unit test, and for that the actual dependencies need to be updated.
In the file "build.gradle" there are dependencies:

~~~
dependencies {
    // Use Apache Log4J for logging
    implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
}
~~~

To the test is able to execute, add the following dependency to the previous dependencies:

~~~
testImplementation 'junit:junit:4.12'
~~~

Then go to the terminal, in the src/ file, and make a directory with the name "Test":

~~~
mkdir Test
~~~

Then go to this new directory and create a new directory with the name "Java":

~~~
mkdir Java
~~~

Apply the previous technique, with this new directory:

~~~
mkdir basic_demo
~~~

At last, create a java class with the name "AppTest.java".

In this "AppTest.java", copy the following test:

~~~
/*
 * This Java source file was generated by the Gradle ’init’ task.
 */
package basic_demo;
import org.junit.Test;
import static org.junit.Assert.*;
public class AppTest {
    @Test public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}
~~~

In this point you want to run the test but there is no need to add a task, because a task to run the test already exists.
In order to check all the existing tasks, just write in the terminal :

* ./gradlew tasks

With this command you will be able to see all the tasks associated to a specific group. And will appear a task named
"test". So, in the terminal, write:

* ./gradlew test

After that, you will see that the test will run and the build has run with success too.


### Fifth Step

In this step is needed to add a task to the "build.gradle" to copy the source folder to a new backup folder.
Try to add a task similar to the previous runServer task. Let's see some pointers:

* the type of this task is to copy a folder;
* it can be added a "group" and a "description";
* in this kind of task is needed a "from" and an "into" line, because it's a copy task.
* NOTE: there is no need to create the "new folder", because the task will make it.

The task will have an appearance like this:

~~~
task copy(type: Copy){
group = "DevOps"
description = "Makes a copy from the src directory"
from "src/"
into "src/copy/src_copy"
}
~~~

In the terminal, you can run it:

* ./gradlew copy

You will be able to see that a new backup folder was created in the src/ folder with the name "Copy" and with all the 
information from the original source file.


### Sixth Step

In this step is needed to add a task to the "build.gradle" to zip a file from the source folder into a new zip file.
Try to add a task similar to the previous runServer task. Let's see some pointers:

* the type of this task is to zip a folder;
* it can be added a "group" and a "description";
* is needed a "from" line;
* in this kind of task is needed a "archiveName" (is the name of the new folder) and a "destinationDir" (the original 
folder that the task will make a zip file) line, because it's a zip task.
* NOTE: there is no need to create the "new folder", because the task will make it.

The task will have an appearance like this:

~~~
task zip(type: Zip){
    group = "DevOps"
    description = "Makes a zip fyle from the src directory"
    from "src/"
    archiveName "src_copy.zip"
    destinationDir(file('build'))
}
~~~

In the terminal, you can run it:

* ./gradlew zip

You will be able to see that a new zipped folder was created in the build folder with the name "src_copy.zip"
and with all the information from the original source file.


### Last Step

When all the previous steps are finished, just do the "old" git command:

~~~
git add .
~~~

To add all the new changes in the project.

~~~
git commit -m"THE MESSAGE FROM THE COMMIT"
~~~

To commit all changes, with the correct message.

~~~
git push
~~~

To push all those changes. 

**NOTE**
Don't forget to add a tag to this assignment (ca2-part1). To do this, write in the terminal:

~~~
git tag ca2-part1
~~~

If the last commit is the commit you want to place the tag, just do:

~~~
git push origin --tags
~~~

If the commit you want to add the tag is not the last one, check the hexadecimal number from the commit and write:

~~~
git tag -a ca2-part1 XPTO
~~~

Replace XPTO to the hexadecimal number. 







