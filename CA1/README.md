# CA 1 - Practice Class:
# Part 1

The main objective in this class is to work with command line and try (or search) the Git commands. 


##Clone the Project

The first step is to clone the project, into your computer. 
It's needed to go to bitbucket and press the "clone" button, then open your terminal and (with the correct commands) 
you should clone the project into the correct fold (i.e. DevOps).

Then you should write de following command:

* git clone https://Jo_Silva@bitbucket.org/jo_silva/devops-21-22-lmn-1211770.git

After that, it's needed to make a directory for de exercise 1 (i.e. CA1) in the command line:

* mkdir CA1

It's important to make a directory for each assigment. 


##Configure your Git

Before the main tasks of the assigment, you should configurate your Git with your name and email.
But first, to check all the pre-configuration, write this command in terminal:

* git config -l

To configurate the name and the email in your Git, write:

* git config --global user.name "YOUR_NAME"
* git config --global user.email YOUR_EMAIL@isep.ipp.pt

After those steps, you will have your Git configurated.

The Git brings a default text editor, but you can configurate a new text editor. For iOS, it is suggested "Emacs" and 
you can use it with this command:

* git config --global core.editor emacs


##Git Basic Command to Daily Use

There are some commands that should be used in a daily basis for update your work in your machine or to update your 
work, when finnished, in the remote repository. To start:

* git pull

With this command, you command you'll update your machine with the last version of the project.
* git fetch

Similar to "git pull", but it's a safer update because this command will pull in all the commits from remote but doesn't 
make any changes to your local files. 
* git add .

or
* git add . "CHOOSE_WHAT_DO_YOU_WANT_TO_ADD_TO_THE_COMMIT"

This command should be used when you want to do a commit but first you need to "add" work to your commit. 
* git commit

or
* git commit -m "WRITE_THE_COMMIT_MESSAGE"

With the first command, will appear the editor text to write the commit message but, with thw command with the message, 
you can write a commit message. The commit message should be related to the work produced in that commit.

* git status

As the name indicates, this commands will show you the status of the git. If the message appears in a red colour, it 
means that the project had some changes. After the "git add .", and writing again "git status", those changes
will appear in a green colour. This means that the status of the git changed to not added to added. 

* git push

This is the last command and the objective is to send your updated work to the remote repository.


##How to check all commits made? 

There are two commads to check all the commits made. It is possible to use:

* git log 

With this will appear first the last commit as the last consequents commits, in a table format.

![git log](https://linuxteaching.com/storage/img/images_3/how_do_i_check_git_logs.png)

* git log --pretty=oneline

With this command will appear all the commits but the information is going to be showed in one line, in a "compact" way.

![git log --pretty=oneline](https://miro.medium.com/max/1400/1*3nzqVtXKxd8CRLLLcAT3wQ.png)


##How to show the last commit information?

With this command will appear all the information (the deleted [red colour], added and changed [green colour] information):

* git show

![git show](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2018/07/20.png)

##How to change the message from the last commit?

If we need to change the message from the last commit, write the following command:

* git commit --amend 

The text editor will open, and the first line correspond to the commit message. Just press "i" (to start the insert 
function) and write the new message. At the end, you need to press "esc", ":" and "x" (to save and close de editor 
text).

##How to create a tag in a commit?

A tag is a mark in the commit to show a change or a new version in the code. To see if there are any tags in the commits
we can do the following command:

* git tag 

The terminal is going to show a list of the existing tags. 

* git tag TAGNAME

To create a tag there is just needed to write the command and the name of the tag. 


##How to create a tag in specific commit?

If is necessary to do a tag in an old commit, it is needed to search the hexadecimal number from the commit (it is the 
first seven numbers). Then we can write:

* git tag -a TAGNAME HEXADECIMAL_NUMBER

This will allow, when the tags are pushed into repository, that the specific commit is now with a tag.


##How to write a message associated to the tag?

It is possible to write a message associated to a tag. For that you can do:

* git tag -a TAGNAME -m"WRITE_THE_MESSAGE"

This tag and message, when pushed, is going to be associated to the last commit. But if it's necessary to write a message
in a tag, from an old commit you can do:

* git tag -a TAGNAME -m"WRITE_THE_MESSAGE" HEXADECIMAL_NUMBER

To check all the previous messages associated to the tags, write:

* git tag -n

A list og the tags and the messages is going to be showed.


##How to delete a tag in a commit?

It depends if the tag is just in your machine or ig the tag is already at the repository.
If the tag is just in your machine you can do the following command:

* git tag -d TAGNAME

Just to be sure, it can be made the command "git tag" to list all the tags and to see if the deleted tag is gone.

* git push --delete origin TAGNAME

This command will delete the referred tag in your repository. Just refresh the web-page and the tag will disappear. 


##How to push the tag to the repository?

After the tag is created, it is needed to push the tag (or tags) into the repository. For that, write the following 
command:

* git push origin --tags


##How to push the project without using git push?

There are a command just to push a specific branch to the origin repository.
git push origin master

* git push origin master

##What is the command to get some help/orientation in Git?

* git help

or
*git help add (i.e.)


It is possible just to write the first command and a list of the basic commands will appear. If the help is more 
specific, it is possible to write de help command and the "origin" of the search. For example, if you want to search 
for the "adding" theme just write "git help add". The last word can be changed to the word of the major doubt.

---

# CA 1 - Practice Class:
# Part 2

The main objective in this class is to work with command line to work with (create, switch, delete) branches.
When we want to add a new feature or fix a bug, but maintain the master branch with the stable versions of the project, 
adding a new branch to work on that is the perfect solution. 


##How to check in which branch we are?

Open the terminal line, on the folder that belongs to the project from bitbucket, and write:

* git branch

Then appear the branch or branches associated to your machine or project, because branches can only be local ou can also
be at the repository too. If there are only a branch, typically, is a master branch.


##How to create a branch?

To create a branch you need to know the name of the new branch and write:

* git branch NAME_OF_BRANCH

At this moment, there are two branch at your local machine, the master branch and the new branch.


##How to delete a branch?

It depends if we want to delete the branch just in our local machine or if it is needed to delete the branch ate the 
remote repository.
To delete a local branch use:

* git branch -d NAME_OF_BRANCH

To check if the branch was deleted, write "git branch" and see all actual branches.

To delete a remote branch use:

* git push --delete NAME_OF_BRANCH

To check if the branch was deleted, refresh the bitbucket web page.


##How to switch from branch to branch?

First you need to see "where you are", it means, in which branch you are. Writhe the command:

* git branch

Then will appear the list of branches. Choose one and, to switch branch, write the following command:

* git checkout NAME_OF_THE_BRANCH

or
* git switch NAME_OF_THE_BRANCH

The change was made and, to be sure, write "git branch" again to check if the command act the correct way.


##How to see the last commits in all branches?

Write the command:

* git branch -v

Then the terminal will show you a list of the last commits in all branches and a * signal with appear on the branch that
we are currently one.

##Note:

If there changes in the branch master and in other branch, it is needed to do the merge in the master branch. To do that
you need to be at the master branch and write the following command:

* git merge NAME_OF_THE_BRANCH

Then it is possible to appear two situations:

1. Automatically merge the changes and those will appear in the terminal.
2. The changes in master branch will appear (in IDE) with the following signal <<<<<<< and the changes in the other 
branch will appear with the signal ==========.


---

#Alternative to Git

Git is a version control system but there are others VCS that can be used. In the search for an alternative, i found 
Mercurial. 

Mercurial is free, open-source and it can handle several projects of any size.

There are some advantages in Mercurial, for example:

1. Mercurial is faster than Git. 
2. It is easy to learn and use.
3. Mercurial does not require maintance (Git needs periodic maintenance for repositories.)

One of the differences from Git and Mercurial, is that Git is Linux based and Mercurial is Phyton based.

Other difference is based on alter history. 
Mercurial allows, with the command "hg rollback", make an "undo" of the previous work. And Git doesn't allow that.

In terms of creating branches, to do a moore productive work, git is more effective in creating branches. With Mercurial,
creating and working with branches can be a little more confusing and, in this case, branches can be removed.

In summary, this image is very helpfull understanding the pros and cons from Git vs Mercurial:

![Git vs Mercurial](https://cdn2.hubspot.net/hubfs/2639771/Imported_Blog_Media/git_mercurial_pros_cons-intland-software.png)

