/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id
	@GeneratedValue
	Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private int jobYears;
	private String email_field;
	private String address;
	private int phone_number;

	private Employee() {
	}

	public Employee(String firstName, String lastName, String description, String jobTitle, int jobYears,
					String email_field, String address, int phone_number) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
		this.jobTitle = jobTitle;
		this.jobYears = jobYears;
		this.email_field = email_field;
		this.address = address;
		this.phone_number = phone_number;
	}

//	@Override
//	public boolean equals(Object o) {
//		if (this == o) return true;
//		if (o == null || getClass() != o.getClass()) return false;
//		Employee employee = (Employee) o;
//		return Objects.equals(id, employee.id) &&
//			Objects.equals(firstName, employee.firstName) &&
//			Objects.equals(lastName, employee.lastName) &&
//			Objects.equals(description, employee.description) &&
//				Objects.equals(jobTitle, employee.jobTitle) &&
//				Objects.equals(jobYears, employee.jobYears) &&
//				Objects.equals(email_field, employee.email_field);
//	}
//
//	//Try to see the change.
//
//	@Override
//	public int hashCode() {
//
//		return Objects.hash(id, firstName, lastName, description, jobTitle, jobYears, email_field);
//	}


	public boolean equals(Object object) {
		if (this == object) return true;
		if (!(object instanceof Employee)) return false;
		if (!super.equals(object)) return false;
		Employee employee = (Employee) object;
		return jobYears == employee.jobYears &&
				phone_number == employee.phone_number &&
				java.util.Objects.equals(id, employee.id) &&
				java.util.Objects.equals(firstName, employee.firstName) &&
				java.util.Objects.equals(lastName, employee.lastName) &&
				java.util.Objects.equals(description, employee.description) &&
				java.util.Objects.equals(jobTitle, employee.jobTitle) &&
				java.util.Objects.equals(email_field, employee.email_field) &&
				java.util.Objects.equals(address, employee.address);
	}

	public int hashCode() {
		return java.util.Objects.hash(super.hashCode(), id, firstName, lastName, description, jobTitle, jobYears,
				email_field, address, phone_number);
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public int getJobYears() {
		return jobYears;
	}

	public void setJobYears(int jobYears) {
		this.jobYears = jobYears;
	}

	public String getEmail_field() {
		return email_field;
	}

	public void setEmail_field(String email_field) {
		this.email_field = email_field;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(int phone_number) {
		this.phone_number = phone_number;
	}


	@java.lang.Override
	public java.lang.String toString() {
		return "Employee{" +
				"id=" + id +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", description='" + description + '\'' +
				", jobTitle='" + jobTitle + '\'' +
				", jobYears=" + jobYears +
				", email_field='" + email_field + '\'' +
				", address='" + address + '\'' +
				", phone_number=" + phone_number +
				'}';
	}
}

//	@Override
//	public String toString() {
//		return "Employee{" +
//			"id=" + id +
//			", firstName='" + firstName + '\'' +
//			", lastName='" + lastName + '\'' +
//			", description='" + description + '\'' +
//				", jobTitle='" + jobTitle + '\'' +
//				", jobYears='" + jobYears + '\'' +
//				", email_field='" + email_field + '\'' +
//			'}';
//	}
//}


// end::code[]
