# CA 4 - Practice Class:
# Part 2

The main objective in this class is to setup a containerized environment to execute a version of the gradle from the 
spring basic tutorial application.

# Tutorial for the exercise (Please try to follow all the steps):

1. Open your "devops" project;

2. Organize your directories and try to create a file named "ReadME.md", inside Part2(CA4);

3. Create a file, inside "Part2", named "docker-compose", without extension;
Docker-compose is a configuration file similar to dockerfile, but this one referes to defining services, networks, and 
volumes for a Docker application.

4. Open your "docker-compose.yml" in IDE and try to edit it, with thw following information:

~~~
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.56.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data-backup
    networks:
      default:
        ipv4_address: 192.168.56.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.56.0/24
~~~

5. The information from docker compose was getted from Prof Alexandre Brangança repository. 

6. In the docker-compose file it is needed to alter some lines:
- Line 9, 21 and 27 needs to be changed to another network (192.168.56) but the port stays the same.

7. Create two folders, one with the name "WEB" and other with name "DB";

8. Inside the "DB" directory, try to create a "Dockerfile" with this information:

~~~
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
~~~

In this file, we don't need to change anything.

9. Inside the "DB" directory, try to create a "Dockerfile" but it is needed to change some infos:

* At the line 23, place your own repository, like this:
~~~
RUN git clone https://Jo_Silva@bitbucket.org/jo_silva/devops-21-22-lmn-1211770.git
~~~

* At the line 25, we need to alter the path that we want to clone follow:

~~~
WORKDIR /tmp/build/devops-21-22-lmn-1211770/CA4/Part2/correct/react-and-spring-data-rest-basic
~~~

* At the line 17, add this line:

~~~
RUN chmod u+x gradlew
~~~

* Because there are some incompatibilities between the version from TOMCAT and the JAVA version (it's a manual installation,
please substitute the two first lines of the doc to this:

~~~
FROM ubuntu:18.04
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install openjdk-8-jdk wget
RUN mkdir /usr/local/tomcat
RUN wget https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.78/bin/apache-tomcat-8.5.78.tar.gz  -O /tmp/tomcat.tar.gz
RUN cd /tmp && tar xvfz tomcat.tar.gz
RUN cp -Rv /tmp/apache-tomcat-8.5.78/* /usr/local/tomcat/

EXPOSE 8080
CMD /usr/local/tomcat/bin/catalina.sh run
~~~

* We are using jdk-11, so please alter line number 5 from jdk-8 to jdk-11;

* At the line 28, add:

~~~
RUN cp build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
~~~

*Note:* The first version of this line is :

~~~
RUN cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
~~~

So, it is needed to change the path to:

~~~
RUN cp build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
~~~

10. Open your terminal and write:

~~~
docker-compose build
~~~

And this will show:

![](images/SpringBoot_1.png)

![](images/SpringBoot_2.png)

To build the project, and the write:

~~~
docker-compose up
~~~

To run the springBoot application.

11. Open your browser and write this link, to access the web:

~~~
http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/
~~~
And this will show:

![](images/WEB.png)

12. Then, open your browser and write this link, to access the db:

~~~
http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console
~~~
And this will show:

![](images/BD.png)

![](images/Open_BD.png)

13. Now we are going to tag the images and sent them to the repository.

14. Write the tags:

~~~
docker tag part2_web 1211770/part2_web:3.0 
~~~

And

~~~
docker tag part2_db 1211770/part2_db:4.0
~~~

15. Then you can push the images to the repository.

16. Push the first image:

~~~
docker push 1211770/part2_db:4.0 
~~~

And, the second one:

~~~
docker push 1211770/part2_web:3.0 
~~~

17. Now we are going to make a copy from DB, using a volume:

~~~
docker compose exec db bash
~~~

This will create a folder in the project to place the volume. If we check the docker-compose file, in the lines 17 and 
18, we are going to see:

~~~
volumes:
- ./data:/usr/src/data-backup
~~~

18. At least please, do:

~~~
cp jpadb.mv.db /usr/src/data-backup
~~~

![](images/Volumes.png)

19. Now you can check that the file was created and if you go to the docker hub you will see that the images was created:

![](images/Last_image.png)

20. Please, do the last commands to add, commit and push your work to the repository:

~~~
git add .

git commit -m'MESSAGE'

git push
~~~
