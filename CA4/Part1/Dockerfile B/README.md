# CA 4 - Practice Class:
# Part 1 - Part B

The main objective in this class is to practice with Docker and Containers.
In this class is to try the same exercise like the part1 but with a copy from the jar in the basic_demo.

# Tutorial for the exercise (Please try to follow all the steps):

1. Open your "devops" project and create a text file (without extension) (for example in MACOS, you can create
   a file, with the text editor but delete de .rft extension);

2. Open your subfolder "Part1" and create a directory named "Dockerfile B".

3. Inside of de "Dockerfile B", place a text file (that you should create);

4. Open your "Dockerfile" in IDE and try to edit it, with thw following information:

~~~
#Start the dockerfile

FROM ubuntu:18.04

#Add new configurations
RUN apt-get update
RUN apt-get install -y openjdk-11-jdk-headless

COPY basic_demo-0.1.0.jar .

#Server Port
EXPOSE 59001

#Run Server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
~~~

5. Open you terminal and make a login in docker:

~~~
docker login
~~~

![](Images/docker_login.png)

6. Then try to run:

~~~
docker images
~~~

![](Images/docker_images.png)

7. To tag the images from the repository, try:

~~~
docker tag 1211770/gradle-basic-demo 372634f765d2:1.0
 
AND

docker tag 1211770/gradle-basic-demo 794e1bae067c:2.0
~~~

*NOTE* If you made a mistake making the tagging in the images, you can always remove it. Here's how:

~~~
docker rmi 1211770/gradle-basic-demo 794e1bae067c:latest (TAG NAME)
~~~

![](Images/docker_rmi.png)

8. Al last to the docker push, to send the images to the repository:
~~~
docker push 1211770/gradle-basic-demo:latest

AND 

docker push 1211770/gradle-basic-demo:2.0
~~~

![](Images/docker_images_push.png)

Or you can use:

~~~
docker push 1211770/gradle-basic-demo 
~~~

![](Images/docker_push.png)


9. Let's check in the docker hub: 

![](Images/Last_Image.png)