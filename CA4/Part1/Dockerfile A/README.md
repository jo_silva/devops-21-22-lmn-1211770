# CA 4 - Practice Class:
# Part 1 - Part A

The main objective in this class is to practice with Docker and Containers.

# What is Docker?

Docker is an open source plataform that makes containers. It allows to standardize containers components combining 
application source code with the operating system (OS) libraries and dependencies required to run that code in any 
environment.

# What is a Container? 

A Docker container is a standalone, executable package of software that includes everything needed to run an 
application: code, runtime, system tools, system libraries and settings.

# Tutorial for the exercise (Please try to follow all the steps):

1. Install docker throw https://www.docker.com/products/docker-desktop/
Use the Docker installation according your OS.

2. Do a "Sign up" in Docker using ISEP credentials;

3. After that, open your "devops" project and create a text file (without extension) (for example in MACOS, you can create
a file, with the text editor but delete de .rft extension);

4. Organize your directories and try to create a folder named "CA4", a subfolder "Part1" and "Part2".

5. Create a file, inside "Part1", named "Dockerfile A";

6. Inside of de "Part1", place you text file (created in step 3.);

7. Name this text file "Dockerfile";
Dockerfile is a configuration file that contains all the commands to assemble an image.

8. Open your "Dockerfile" in IDE and try to edit it, with thw following information:

~~~
#Start the dockerfile
FROM ubuntu:18.04

#Add new configurations
RUN apt-get update
RUN apt-get install -y openjdk-11-jdk-headless
RUN apt-get install -y git
RUN git clone https://Jo_Silva@bitbucket.org/luisnogueira/gradle_basic_demo.git

#Work in this directory
WORKDIR gradle-basic-demo/
RUN chmod u+x gradlew
RUN ./gradlew clean build

#Server Port
EXPOSE 59001

#Run Server
CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
~~~

*Some Notes:*

- The "FROM ubuntu:18.04" it's used to install a jkd11 and, throw the ubuntu image, we are going to build a docker hub;
- All the commands (FROM, RUN, EXPOSE, CMD, ...) need to be written in upper case to initialize the commands.
- Docker Hub is a central server where are all the images.

*End of the notes*

8. Open your terminal and write the following command:

~~~
docker build -t 1211770/gradle-basic-demo .
~~~

This will allow to build the dockerfile throw our username (1211770 is my username in docker);

9. Then try to run:

~~~
docker images
~~~

![](Images/docker_images.png)


This will allow to check if the image was created.

10. To launch the container, run:

~~~
docker run -p 59001:59001 1211770/gradle-basic-demo 
~~~

![](Images/run_server.png)

*NOTE* Is possible to add "-d" (detached) to be possible to continue to use the same terminal to continue the tasks.

11. To launch the container, we need to launch all their tasks (to run the server)

12. Write in a new terminal the command, to check all images:

~~~
docker ps -a
~~~

![](Images/dpcker_ps-a.png)


13. Do the clone of the repository from Prof. Luís Nogueira:
~~~
git clone https://Jo_Silva@bitbucket.org/luisnogueira/gradle_basic_demo.git
~~~

14. Write the following commands to build and then run the task runClient:

~~~
./gradlew build

and

./gradlew runClient
~~~

![](Images/chat_room.png)

15. Then the chat room will open:

![](Images/the_chat_room.png)

16. At last, do this command to the docker recognize the user:

~~~
docker login
~~~

17. The last step is to push all your work at the docker hub:

~~~
docker push 1211770/gradle-basic-demo
~~~

It may take a little time to run all the images and all the work.
If you check your docker hub, you will find:

![](Images/docker_push.png)



