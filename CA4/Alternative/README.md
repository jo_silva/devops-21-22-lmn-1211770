# CA 4 - Practice Class:
# Alternative

# Kubernetes 

Kubernetes (known has K8s) is an open-source container platform for managing and automating containerized applications.
It is very extensible and automates a ton of common operations. It addresses the need for enterprises to 
commonly orchestrate container deployment. A lot of organizations user Kubernetes to the deployment and management step 
for their applications.

# Docker vs Kubernetes

![](images/1.png)

Both Docker and Kubernetes are production-grade container platforms, although they have different strengths.

Docker also referred to as Docker in swarm mode, is the easiest orchestrator to deploy and manage. It can be a 
good choice for an organization just getting started with using containers in production. Docker solidly covers 80% of 
all use cases with 20% of Kubernetes complexity.

Docker and Kubernetes can be used independently. Whereas a large enterprise may benefit from Kubernetes and can support 
its maintenance, a smaller project may benefit from just adopting Docker. Or, a company may utilize Docker containers 
with another container scheduler. Similarly, Kubernetes is most commonly used with Docker containers, but it can work 
with other container types and runtimes.

![](images/vs.png)

# Conclusion

Both Docker and Kubernetes have emerged to respond to the needs of microservices development. In this paradigm, teams 
must rapidly iterate and deliver highly available services to end users. Containers are a lightweight, scalable way to 
deliver these applications, but managing them at scale poses challenges.